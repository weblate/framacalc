[![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/48px-GitLab_Logo.svg.png)](https://framagit.org)

🇬🇧 English: **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

🇫🇷 Français : **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

Framacalc est un tableur collaboratif en ligne que Framasoft propose sur le site : https://framacalc.org

Il repose sur le logiciel [Ethercalc](https://ethercalc.net/) que nous avons à peine retouché : traduction (intégrée officiellement dans ethercalc), page d'accueil et ajout de la [Framanav](https://framagit.org/framasoft/framanav) dans les fichiers `index.html` et `multi/index.html` d’Ethercalc (bien que ce dernier ne soit pas utilisé vu que nous avons désactivé les calcs multi-feuille).

Ce dépôt ne contient pas Ethercalc, juste la page d’accueil.
* * *